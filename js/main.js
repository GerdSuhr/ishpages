// set tabnav menu to fixed position on scroll down
var position = $(window).scrollTop();
$(window).scroll(function() {
	var scroll = $(window).scrollTop();
	if (scroll > position) {
		$('#tabnav').css('top', '0px');
	} else {
		$('#tabnav').css('top', '43px');
	}
	position = scroll;
});


// Add smooth scrolling to all anchor links
// On ishop we might be able to use the existing smooth scroll JS
// however we will need to add additional offset detection for the fixed height tabnav menu

$('.tabs a').on('click', function(event) {
	// add active class
	$('#tabnav li').removeClass('active');
	$(this)
		.parent()
		.addClass('active');

	// Make sure this.hash has a value before overriding default behavior
	if (this.hash !== '') {
		// Prevent default anchor click behavior
		event.preventDefault();
		// Store hash
		var hash = this.hash;
		var scrollpos = $(document).scrollTop();
		// adjust for our fixed nav heights (if both menus showing, 90px. if only navtabs showing, 47px)
		scrollpos < 100
			? (offset = $(hash).offset().top - 90)
			: (offset = $(hash).offset().top - 47);

		$('html, body').animate(
			{
				scrollTop: offset,
			},
			300,
			function() {
				// Add hash (#) to URL when done scrolling (mimic default click behavior)
				window.location.hash = hash;
			}
		);
	} // End if
});

$('#tabnav #menu-mobile li').on('click', function() {
	$('#checkbox-toggle').prop('checked', false);
});

// right slideout panel click event
$('.slideview').on('click', function() {
	$('#product-slide-toggle').prop('checked', true);
});

// var resizeTimer;
// $(window).on('resize', function(e) {
//      on resize code goes here
//   clearTimeout(resizeTimer);
//   resizeTimer = setTimeout(function() {
//   }, 250);
// });
